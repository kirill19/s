#include <stdio.h>
#include <windows.h>
#include "Header.h"
#include <direct.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <conio.h>
#include "pprog.cpp"


void test() {
	char name[255];
	FILE* f = NULL;
	char code;
	bool flag = false;
	do {
		printf("1)Test one file;\n");
		code = getch();
		system("cls");
		if(code == '1') {
			do {
				int res = 0;
				char seq[255];
				printf("Enter way and filename: ");
				scanf("%s", name);
				system("cls");
				int seqLen = strlen(seq);
				printf("Enter way and filename: ");
				int errCode = func(name, seq, res, seqLen);
				f = fopen("log.txt", "a");
				fprintf(f, "Name: %s; Result: %d.\n", name, res);
				fclose(f);
			} while(f == NULL);
		}
	} while(code != '1');
	
}

void Filling(double chance, int max, FILE* f) {
	double y;
	char x;
	for(int i = 0; i < max; i++) {
		y = (double)rand()/RAND_MAX;
		if(y <= chance)
			do {
				x = rand() % 126 + 33;
			} while(x > 126 || x < 1);
		else
			do {
				x = rand() % 126 + 33;
			} while(x <= 126 && x >= 1);	
		fprintf(f, "%c", x);
	}
}

void Name(char* name) {
	do {
		printf("Enter file's name (3-12 symbols): ");
		scanf("%s", name);
		system("cls");
	} while(strlen(name) < 3 || strlen(name) > 12);
}

void Chance(double& chance) {
	int err;
	do {
		printf("Enter CHANCE genereration (0..1): ");
		fflush(stdin);
		err = scanf("%lf", &chance);
		system("cls");
	} while(chance < 0 || chance > 1 || err == 0);
}

void MaxLength(int& max) {
	int err;
	do {
		printf("Enter MAX length (>0 and <1125): ");
		fflush(stdin);
		err = scanf("%d", &max);
		system("cls");
	} while(max <= 0 || max >= 1125 || err == 0);
}

void Manually() {
	char name[32];
	int max, err;
	MaxLength(max);
	Name(name);
	int res = mkdir("other");
	char loc[32];
	strcat(loc, "other/");
	strcat(loc, name);
	FILE* f = NULL;
	f = fopen("log.txt", "w+");
	fprintf(f, "MAX: %d; \n: 1.\n\n", max);
	fclose(f);
	f = NULL;
	f = fopen(strcat(loc, ".txt"), "w+");
	if(f != NULL) {
		for(int i = 0; i < max; i++) {
			char c;
			do {
				printf("Element %d: ", i);
				fflush(stdin);
				err = scanf("%c", &c);
				if(err != 0) {
					fprintf(f, "%c", c);
				}	
			} while(err == 0);
		}
		printf("Manually success!");
		getch();
		system("cls");
		fclose(f);
	} else {
		printf("File Error!");
		assert(0);
	}
}

void Randomly() {
	int err, max 
	char name[32];
	double chance;
	Chance(chance);
	MaxLength(max);
	
	Name(name);
	char way[32] = ""; 
	strcat(way, name);
	strcat(way, "/");
	strcat(way, name);
	int res = mkdir(name);
	FILE* f = NULL;
	f = fopen(strcat(name, "_log.txt"), "w+");
	fprintf(f, "MAX: %d;\n\n\n", max);
	int i = 0;
	do {
		fclose(f);
		f = NULL;
		char loc[32] = "";
		char num[32] = "";
		strcat(loc, way);
		sprintf(num, "%d", i);
		strcat(loc, num);
		strcat(loc, ".txt");
		f = fopen(loc, "w+");
		if(f != NULL) {
			Filling(chance, max, f);
		}
		i++;
	} while(f != NULL);
	fclose(f);
	system("cls");
	printf("End of generation...");	
	getch();
	system("cls");
}

void gener() {

	char code;

	do {
		code = '0';
		printf("Generation:\n\n");
		printf("1)Randomly\n");
		printf("2)Manually\n");
		printf("3)Back\n");
		code = getch();
		system("cls");
		if(code == '1') {
			Randomly();
		} else if(code == '2') {
			Manually();
		}
	} while(code != '3');
}

int main() {

	char code;
	char seq =0;

	do {
		code = '0';
		printf("Menu:\n\n");
		printf("0)sequence\n");
		printf("1)Generation\n");
		printf("2)Test\n");
		printf("3)Exit\n");
		code = getch();
		system("cls");
		if(code=='0'){
			printf("Enter sequence: ");
			scanf("%s", seq);
		}
		if(code == '1') {
		gener();
		} else if(code == '2') {
			test();
		}
	} while(code != '3');
	
	return 0;
}
