#include <stdio.h>
#include <stdbool.h>
#include <windows.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "error.cpp"

const int indentation = 5, indentation_point = 7;

typedef double elem_type;

typedef struct {
    int n;
    int m;
    elem_type **arr;
    char *name;
} table_type;

#define null_table (table_type){0, 0, NULL, NULL}

bool table_valid (const table_type *table);

bool table_assay (char *name) {
	char **local = NULL;
	int i = 0;
	int n = table_list_array(&local);
	bool f;
	
	char local_name [256];
	strcpy (local_name, name);
	strcat (local_name, ".tbl");
	while (i < n && !f) {
        f = strcmp(local[i], local_name) == 0;
        i++;
	}
	for (i = 0; i < n; i++)
        free(local[i]);
    free(local);
	return f;
}

void table_create_new (table_type *table, bool *save) {
	char codekey = 0;
	char* name = calloc (sizeof (char), 16);
	int i, j, n, m, error;
	table_free (table);

	do {
		printf ("\n");
		printf ("%*sEnter rows and columns throw space: ", indentation, "");
		fflush(stdin);
		error = scanf ("%d %d", &n, &m);
		system ("cls");
	} while (error == 0 || n <= 0 || m <= 0);

	do {
		printf ("\n");
		printf ("%*sEnter name not more 12 symbols: ", indentation, "");
		fflush(stdin);
		scanf ("%s", name);
		system ("cls");
	} while (strlen (name) > 12 || strlen (name) <= 0);
	*table = (table_type){n, m, NULL, name};
	table->arr = malloc (sizeof (elem_type*) * table->n);
	for (i = 0; i < table->n; i++)
		table->arr[i] = malloc(sizeof(elem_type) * table->m);

	do {
		printf ("\n");
		printf ("%*sSelect type of creating table!\n", indentation, "");
		printf ("%*s1) Random;\n", indentation, "");
		printf ("%*s2) Manually.\n", indentation, "");
		codekey = getch();
		system ("cls");
		if (codekey == 49) {
			table_create_random(table);
		} else if (codekey == 50) {
			table_create_manually(table);
		}
	} while (codekey != 49 && codekey!= 50);
	*save = false;
}

void table_create_manually (const table_type *table) {
	int i, j;
	for (i = 0; i < table->n; i++) {
		for (j = 0; j < table->m; j++) {
			int error;
			do {
				printf ("\n\n");
				printf ("%*s%s%d%c%d%s", indentation, "", "Enter an element with coordinates (", i, ';', j, "): ");
				fflush (stdin);
				error = scanf ("%lf", &table->arr[i][j]);
				system ("cls");
			} while (error == 0);
		}
	}
	printf ("\n\n");
	printf ("%*sThe table was successfully populated\n", indentation, "");
	getch();
}

void table_create_random (const table_type *table) {
	int i, j;
	for (i = 0; i < table->n; i++) {
		for (j = 0; j < table->m; j++) {
			table->arr[i][j] = (rand() / (elem_type)RAND_MAX) * 10;
		}
	}
	printf ("\n\n");
	printf ("%*sThe table was successfully populated\n", indentation, "");
	getch();
}

void table_enter_name (char *name) {
	char **local = NULL;
	int i;
	int n = table_list_array(&local);
	bool f;
	do {
		printf ("\n\n");
		printf ("%*s***Enter \"exit()\" for back to menu***\n", indentation, "");
		if (f) {
			printf ("%*sEnter name not more 12 symbols: ", indentation, "");
		} else {
			printf ("%*sInvalid string or file is absent, repeat enter: ", indentation, "");
		}

		fflush(stdin);
		scanf ("%s", name);
		system ("cls");
		i = 0;
		f = false;
		char local_name [256];
		strcpy (local_name, name);
		strcat (local_name, ".tbl");
		while (i < n && !f) {
            f = strcmp(local[i], local_name) == 0;
            i++;
		}
	} while ((strlen (name) > 12 || strlen (name) <= 0 || !f) && strcmp(name, "exit()") != 0);
	for (i = 0; i < n; i++)
        free(local[i]);
    free(local);
}

void table_filling (table_type *table, char *name, bool *save) {
	char local_name[256] = "tables/";
	strcat (local_name, name);
	strcat (local_name, ".tbl");
	FILE *pFile = fopen(local_name, "rb");
	int i, n, m;
	if (pFile == NULL) {
		printf ("\n");
		Error ();
		printf ("\n\n");
		printf ("%*sFile is Null", indentation, "");
		getch ();
	} else {
		int local1 = fread (&table->n, sizeof (table->n), 1, pFile),
        	local2 = fread (&table->m, sizeof (table->m), 1, pFile); 
		if (local1 != 0 && local2 != 0) {
			if (table->n > 0 && table->m > 0) {
				*table = (table_type){table->n, table->m, NULL, name};
				table->arr = malloc (sizeof (elem_type*) * table->n);
				for (i = 0; i < table->n; i++) {
					table->arr[i] = malloc(sizeof(elem_type) * table->m);
					fread(table->arr[i], sizeof(elem_type), table->m, pFile);
				}
				printf ("%*sSuccess filling! Press anykey.", indentation, "");
				getch ();
			} else {
				printf ("\n");
				Error ();
				printf ("\n\n");
				printf ("%*sCols or Rows is negative", indentation, "");
				getch ();	
			}	
		} else {
			printf ("\n");
			Error ();
			printf ("\n\n");
			printf ("%*sCols or Rows isn't number", indentation, "");
			getch ();	
		}
		fclose (pFile);
		*save = true;
	}
	system ("cls");
}

void table_free (table_type *table) {
    int i;
    if (table_valid (table)) {
        for (i = 0; i < table->n; i++)
            free(table->arr[i]);
        free(table->arr);
        free(table->name);
        *table = null_table;
    }
}

void table_list() {
	WIN32_FIND_DATA FindFileData;
	HANDLE hf;
	hf = FindFirstFile ("tables/*.tbl", &FindFileData);
	int i = 0;
	if (hf != INVALID_HANDLE_VALUE)
	{
		do {
			char *name = FindFileData.cFileName;
			printf ("%*s%s\n", indentation, "", name);
			i++;
		}
		while (FindNextFile (hf, &FindFileData)!=0);
		FindClose (hf);
	}
	getch();
}

int table_list_array (char ***arr) {
	WIN32_FIND_DATA FindFileData;
	HANDLE hf = FindFirstFile("tables/*.tbl", &FindFileData);
	int i = 0, n = table_list_itterator();
	*arr = malloc (sizeof (char*) * n);
	if (hf != INVALID_HANDLE_VALUE) {
		do {
			(*arr)[i] = calloc(sizeof(char), strlen(FindFileData.cFileName) + 1);
            strcpy ((*arr)[i], FindFileData.cFileName);
			i++;
		}
		while (FindNextFile(hf, &FindFileData) != 0);
		FindClose(hf);
	}
	return n;
}

int table_list_itterator() {
	WIN32_FIND_DATA FindFileData;
	HANDLE hf;
	hf = FindFirstFile ("tables/*.tbl", &FindFileData);
	int i = 0;
	if (hf != INVALID_HANDLE_VALUE)
	{
		do {
			char *name = FindFileData.cFileName;
			i++;
		}
		while (FindNextFile (hf, &FindFileData)!=0);
		FindClose (hf);
	}
	return i;
}

int table_perform (table_type *table, elem_type *res) {
	bool f = table_valid (table), flag = false;
	int k, i = 0, j = 0, i1 = 0, j1 = 0, code = 0;
	printf("\n");
	if (f) {
		while (i < table->n && !flag) {
			while (j < table->m && !flag) {
				i1 = 0;
				flag = true;
				while (i1 < table->n && flag) {
					j1 = 0;
					flag = false;
					while (j1 < table->m && !flag) {
						if (table->arr[i][j] == table->arr[i1][j1])
							flag = true;
						j1++;
					}
					i1++;
				}
				j++;
			}
			i++;
		}
		if (flag) {
			*res = table->arr[i-1][j-1];
			code = 1;
		} else {
			code = 2;
		}
		printf ("%*sTable was performed.", indentation, "");
		printf("\n");
		printf("\n");
	} else {
		printf ("%*sSorry, but you did not fill the table.\n", indentation, "");
		printf ("%*sSelect menu item 2 for fill table\n", indentation, "");
	}
    return code;
}

char table_quit(table_type *table, bool *save) {
	char c = 54;
    if (table_valid(table)) {
    	if (!*save) {
			char codekey = 0;
	    	do {
		    	printf("\n");
		    	printf ("%*sTable wasn't saved. Do you want to save table?\n", indentation, "");
				printf ("%*s'Y' - do save and quit;\n", indentation, "");
				printf ("%*s'N' - not save, quit;\n", indentation, "");
				printf ("%*s'C' - return menu...\n", indentation, "");
				codekey = getch ();
				system ("cls");
				if (codekey == 'Y' || codekey == 'y') {
					table_save (table, save);
				} else if (codekey == 'C' || codekey == 'c') {
					c = 0;
				}	
			} while (codekey != 'Y' && codekey != 'y' && codekey != 'N' && codekey != 'n' && codekey != 'C' && codekey != 'c');
		}
	} 
	return c;
}

void table_save (table_type *table, bool *save) {
	if (table_valid (table)) {
		if (!*save) {
			if (table_assay (table->name)) {
				char codekey = 0;
				do {
					printf("\n");
					printf ("%*sName \"%s\" is engaged now. Save anyway?\n", indentation, "", table->name);
					printf ("%*s'Y' - save, rewrite;\n", indentation, "");
					printf ("%*s'N' - not save;\n", indentation, "");
					printf ("%*s'R' - enter other name...", indentation, "");
					codekey = getch ();
					system ("cls");
					printf("\n");
					if (codekey == 'Y' || codekey == 'y') {
						char name[256] = "tables/";
						int i;
				   		strcat (name, table->name);
				    	strcat (name, ".tbl");
						FILE *f = fopen(name, "wb+");
					    if (f != NULL) {
					        fwrite(&(table->n), sizeof(table->n), 1, f);
					        fwrite(&(table->m), sizeof(table->m), 1, f);
					        for (i = 0; i < table->n; i++) {
					            fwrite(table->arr[i], sizeof(elem_type), table->m, f);
					        }
					        fclose(f);
					    }
					    *save = true;
					    printf ("%*sSuccess save.\n", indentation, "");
					} else if (codekey == 'R' || codekey == 'r') {
						do {
							printf ("\n");
							printf ("%*sEnter name not more 12 symbols: ", indentation, "");
							fflush(stdin);
							scanf ("%s", table->name);
							system ("cls");
						} while (strlen (table->name) > 12 || strlen (table->name) <= 0);
					}	
				} while (codekey != 'Y' && codekey != 'y' && codekey != 'N' && codekey != 'n' && table_assay (table->name));
				if (!table_assay (table->name)) {
					char name[256] = "tables/";
					int i;
					strcat (name, table->name);
					strcat (name, ".tbl");
					FILE *f = fopen(name, "wb+");
					if (f != NULL) {
						fwrite(&(table->n), sizeof(table->n), 1, f);
						fwrite(&(table->m), sizeof(table->m), 1, f);
						for (i = 0; i < table->n; i++) {
						    fwrite(table->arr[i], sizeof(elem_type), table->m, f);
						}
						fclose(f);
					}
					*save = true;
					printf("\n");
					printf ("%*sSuccess save.\n", indentation, "");
				}
			} else {
				char name[256] = "tables/";
				int i;
				strcat (name, table->name);
				strcat (name, ".tbl");
				FILE *f = fopen(name, "wb+");
				if (f != NULL) {
					fwrite(&(table->n), sizeof(table->n), 1, f);
					fwrite(&(table->m), sizeof(table->m), 1, f);
					for (i = 0; i < table->n; i++) {
					    fwrite(table->arr[i], sizeof(elem_type), table->m, f);
					}
					fclose(f);
				}
				*save = true;
				printf("\n");
				printf ("%*sSuccess save.\n", indentation, "");
			}
		} else {
			printf("\n");
			printf ("%*sTable already was saved or wasn't changed.\n", indentation, "");
		}
	} else {
		printf("\n");
		printf ("%*sSorry, table is absent. Take table from file or create new.\n", indentation, "");
	}
	printf ("%*sPress any key for continue...", indentation, "");
	getch ();
}

void table_select (table_type *table, bool *save) {
	char *name = calloc (sizeof (char), 256);
	char codekey = 0;
	do {
		printf ("\n\n");
		printf ("%*sSelect that you want to do:\n", indentation, "");
		printf ("%*s1) Print list of all tables;\n", indentation, "");
		printf ("%*s2) Enter name table that need;\n", indentation, "");
		printf ("%*s3) Back to Main Menu.\n", indentation, "");
		codekey = getch();
		system ("cls");
		if (codekey == 49) {
			printf ("\n");
			table_list();
		} else if (codekey == 50) {
			table_warning (table, save);
			table_enter_name (name);
			printf ("\n");
			if (strcmp(name, "exit()") != 0) {
				printf ("%*sName table is \"%s\"\n", indentation, "", name);
				table_filling (table, name, save);
			}
		}
		system ("cls");
	} while (codekey != 51 && codekey != 50);
}

void table_show (const table_type *table) {
	int i, j;
	printf ("\n\n");
	if (table_valid (table)) {
	    printf("%*sTable \"%s\" %dx%d:\n", indentation, "", table->name, table->n, table->m);
	    for (i = 0; i < table->n; i++) {
	    	printf ("%*s", indentation, "");
	        for (j = 0; j < table->m; j++)
	            printf("%10g ", table->arr[i][j]);
	        printf("\n");
	    }
	} else {
		printf ("%*sSorry, but you did not fill the table.\n", indentation, "");
		printf ("%*sSelect menu item 2 for fill table\n", indentation, "");
	}
	printf("\n");
    printf ("%*sPress any key for continue...", indentation, "");
    getch();
}

bool table_valid (const table_type *table) {
    bool f = false;
    if (table != NULL) {
        f = table->arr != NULL && table->name != NULL &&
              table->n > 0 && table->m > 0;
    }
    return f;
}

void table_warning(table_type *table, bool *save) {
    if (table_valid(table)) {
    	if (!*save) {
			char codekey = 0;
	    	do {
		    	printf("\n");
		    	printf ("%*sTable wasn't saved. Do you want to save table?\n", indentation, "");
				printf ("%*s'Y' - do save and go to create new;\n", indentation, "");
				printf ("%*s'N' - not save, go to create new instead old...\n", indentation, "");
				codekey = getch ();
				system ("cls");
				if (codekey == 'Y' || codekey == 'y') {
					table_save (table, save);
				}
			} while (codekey != 'Y' && codekey != 'y' && codekey != 'N' && codekey != 'n');
		}
	} 
	system ("cls");	
}

int main (void) {
	srand (time (0));
	char codekey = 0;
	int i = 0;
	elem_type result = 0;
	table_type table = null_table;
	bool save = true;
	do {
		system ("cls");
		char name [20];
		if (table.name == NULL) {
			strcpy (name, "table isn't choosed");
		} else {
			strcpy (name, table.name);
		}
		printf ("\n\n");
		printf ("%*sWelcome, user!\n", indentation, "");
		printf ("%*sThe program is at your disposal.\n\n", indentation, "");
		printf ("%*sTable for using: ***%s***.\n\n", indentation, "", name);
		printf ("%*sMenu:\n", indentation, "");
		printf ("%*s1)Select table from available;\n", indentation_point, "");
		printf ("%*s2)Create a new table;\n", indentation_point, "");
		printf ("%*s3)Show table;\n", indentation_point, "");
		printf ("%*s4)Perform processing of the table and show the result;\n", indentation_point, "");
		printf ("%*s5)Save table;\n", indentation_point, "");
		printf ("%*s6)Quit program.\n\n\n\n", indentation_point, "");
		codekey = getch();
		system ("cls");
		if (codekey == 49) {
			table_select (&table, &save);
		} else if (codekey == 50) {
			table_warning (&table, &save);
			table_create_new (&table, &save);
		} else if (codekey == 51) {
			table_show (&table);
		} else if (codekey == 52) {
			int k = table_perform (&table, &result);
			if (k == 2) {
				printf ("%*sNumber absent!", indentation, "");
			} else if (k == 1) {
				printf ("%*sResult number: %g;", indentation, "", result);
			}
			printf("\n");
			printf ("%*sPress any key for continue...", indentation, "");
			getch();
		} else if (codekey == 53) {
			table_save (&table, &save);
		} else if (codekey == 54) {
			codekey = table_quit (&table, &save);
		}
	} while (codekey != 54);
	table_free (&table);
	printf ("\n\n");
	printf ("%*sThe program is completed.\n", indentation, "");
	printf ("\n");
	return 0;
}
