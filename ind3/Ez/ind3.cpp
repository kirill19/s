#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <conio.h>


int Process (double** table, int n, int m) 
{
	int result = 0;
	if (table != NULL && n > 0 && m > 0) 
	{
		int i, j, k = 0, z;
		int* i_arr;
		int* j_arr;
		
		for (i = 0; i < n; i++) 
		{
			for (j = 0; j < m; j++) 
			{
				if (table[i][j] == 0) {
					k++;
				}
			}	
		}
		
		i_arr = new int [k];
		j_arr = new int [k];
		k = 0;
		
		for (i = 0; i < n; i++) 
		{
			for (j = 0; j < m; j++) 
			{
				if (table[i][j] == 0) {
					i_arr[k] = i;
					j_arr[k] = j;
					k++;
				}
			}	
		}
		
		k = 0;
		
		for (i = 0; i < n; i++) 
		{
			for (j = 0; j < m; j++) 
			{
				if (table[i][j] == 0 && i == i_arr[k] && j == j_arr[k]) {
					for (z = 0; z < n; z++) 
					{
						table[z][j] = 0;
					}
					for (z = 0; z < m; z++) 
					{
						table[i][z] = 0;
					}
					k++;
				}
			}	
		}
		
		if (k == 0) 
		{
			result = 2;
		} 
		else 
		{
			result = 1;
		}
	}
	return result;
}

int main() 
{
	
	char key;
	double** table = NULL;
	int n, m, i, j, pr = 0;
	bool error_flag = false;
	FILE *file = NULL;
	
	do 
	{
		system ("cls");
		printf ("Menu:\n\n");
		printf ("1) Create table\n");
		printf ("2) Process table\n");
		printf ("3) Show result\n");
		printf ("4) Show table\n");
		printf ("5) Save table in file\n");
		printf ("0) Exit\n");
		
		key = getch();
		system ("cls");
		
		if (key == '1') 
		{
			int err;
			do
			{
				system ("cls");
				printf ("Creating table\n\n");
				printf ("1) Random\n");
				printf ("2) Manually\n");
				printf ("3) File\n\n");
				printf ("9) Back to menu\n");
				
				key = getch();
				system ("cls");
				if (key == '1' || key == '2' || key == '3') 
				{
					do 
					{
						printf ("Enter n, m: ");
						fflush (stdin);
						err = scanf ("%d%d", &n, &m);
					} while (err == 0 || n <= 0 || m <= 0);
					table = new double* [n];
					if (table != NULL) 
					{
						if (key == '1') 
						{
							for (i = 0; i < n; i++) 
							{
								table[i] = new double [m];
								for (j = 0; j < m; j++) 
								{
									table[i][j] = rand()%100 - 50;
								}
							}
							printf ("The table was filled with random numbers!\n");
							printf ("Press ENTER...\n");
							key = '9';
							getch();
						}
						else if (key == '2')
						{
							for (i = 0; i < n; i++) 
							{
								table[i] = new double [m];
								for (j = 0; j < m; j++) 
								{
									err = 0;
									do
									{
										system ("cls");
										fflush(stdin);
										printf ("Enter element with cord (%d; %d): ", i, j);
										err = scanf ("%lf", &table[i][j]);
									} while (err == 0);
								}
							}
							printf ("The table was filled manually!\n");
							printf ("Press ENTER...\n");
							key = '9';
							getch();
						}
						else if (key == '3')
						{
							file = fopen ("extract.txt", "r");
							if (file != NULL)
							{
								for (i = 0; i < n; i++) 
								{
									table[i] = new double [m];
								}
								i = 0;
								while (fscanf (file, "%lf", &table[i/m][i%m]) != 0 && i < n*m-1 && !feof(file)) 
								{
									i++;
									
								}
								if (i == n*m-1)
								{
									printf ("The table was filled from file!\n");
									printf ("Press ENTER...\n");
									key = '9';
								}
								else
								{
									printf ("File's elements invalid!\n");
									printf ("Press ENTER...\n");	
								}
								fclose (file);
							}
							else
							{
								printf ("File invalid!\n");
								printf ("Press ENTER...\n");
							}
							getch();
						}
					} 
					else
					{
						error_flag = true;
					}
				}
			} while (key == '9' && error_flag);
		}
		else if (key == '2')
		{
			pr = Process (table, n, m);
			if (pr == 0) 
			{
				printf ("Processing error!\n");
			} 
			else if (pr == 1)
			{
				printf ("Success processed!\n");
			} 
			else if (pr == 2)
			{
				printf ("Zeros is absent!\n");
			}
			printf ("Press ENTER...\n");
			getch();
		}
		else if (key == '3')
		{
			if (table != NULL) 
			{
				if (pr != 0) 
				{
					printf ("Table processed:\n\n");	
					for (i = 0; i < n; i++) 
					{
						for (j = 0; j < m; j++) 
						{
							printf ("%4.0lf ", table[i][j]);
						}
						printf ("\n");	
					}
					printf ("\n");
				}
				else
				{
					printf ("Process please!\n");	
				}
			} 
			else 
			{
				printf ("The table wasn't filled!\n");		
			}
			printf ("Press ENTER...\n");
			getch();
		}
		else if (key == '4')
		{
			if (table != NULL) 
			{
				printf ("Table:\n\n");	
				for (i = 0; i < n; i++) 
				{
					for (j = 0; j < m; j++) 
					{
						printf ("%4.0lf ", table[i][j]);
					}
					printf ("\n");	
				}
				printf ("\n");
			} 
			else 
			{
				printf ("The table wasn't filled!\n");		
			}
			printf ("Press ENTER...\n");
			getch();
		}
		else if (key == '5')
		{
			if (table != NULL) 
			{
				file = fopen ("save.txt", "w+");
				if (file != NULL)
				{
					for (i = 0; i < n; i++) 
					{
						for (j = 0; j < m; j++) 
						{
							fprintf (file, "%4.0lf ", table[i][j]);
						}
						fprintf (file, "\n");	
					}
					printf ("The table was recorded to a file!\n");
					fclose (file);
				}
				else
				{
					printf ("File invalid!\n");
				}
			}
			else
			{
				printf ("The table wasn't filled!\n");
			}
			printf ("Press ENTER...\n");
			getch();
		}
		
	} while (key != '0' && !error_flag);
	
	if (table != NULL)
	{
		for (i = 0; i < n; i++) 
		{
			delete[] table[i];
		}
		delete[] table;
	}
	
	if (error_flag)
	{
		printf ("An error has occurred!");
	}
	else
	{
		printf ("The program completed successfully!");
	}
	return 0;
}
