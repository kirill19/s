#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <conio.h>


int Process1 (double** table, int n, int m) 
{
	int b=0,sum=0;
	if (table != NULL && n > 0 && m > 0) 
	{
		int i, j;	
		for (i = 0; i < n; i++) 
		{
			for (j = 0; j < m; j++) 
			{
				b=b+abs(table[i][j]);
				if (b>sum) {
					sum=b;
				}
			}
			b=0;
				
		}	
	}
	return sum;
}

int Process2 (double** table, int n, int m,int sum) 
{
	int b=0,u=0;
	if (table != NULL && n > 0 && m > 0) 
	{
		int i, j;	
		for (i = 0; i < n; i++) 
		{
			for (j = 0; j < m; j++) 
			{
				b=b+abs(table[i][j]);
				
			}
			if (b!=sum) 
				{
					u++;
					
				}
			b=0;		
		}	
	}
	return u;
}


int main() 
{
	
	char key;
	double** table = NULL;
	int n, m, i, j, pr = 0,u=0,sum=0;
	bool error_flag = false;
	FILE *file = NULL;
	
	do 
	{
		system ("cls");
		printf ("Menu:\n\n");
		printf ("1) Create table\n");
		printf ("2) Process table\n");
		printf ("3) Show result\n");
		printf ("4) Show table\n");
		printf ("5) Save table in file\n");
		printf ("0) Exit\n");
		
		key = getch();
		system ("cls");
		
		if (key == '1') 
		{
			int err;
			do
			{
				system ("cls");
				printf ("Creating table\n\n");
				printf ("1) Random\n");
				printf ("2) Manually\n");
				printf ("3) File\n\n");
				printf ("9) Back to menu\n");
				
				key = getch();
				system ("cls");
				if (key == '1' || key == '2' || key == '3') 
				{
						if (key == '1') 
						{
							do 
							{	
								printf ("Enter n, m: ");
								fflush (stdin);
								err = scanf ("%d%d", &n, &m);
							} while (err == 0 || n <= 0 || m <= 0);
								table = new double* [n];
							for (i = 0; i < n; i++) 
							{
								table[i] = new double [m];
								for (j = 0; j < m; j++) 
								{
									table[i][j] = rand()%100 - 50;
								}
							}
							printf ("The table was filled with random numbers!\n");
							printf ("Press ENTER...\n");
							key = '9';
							getch();
						}
						else if (key == '2')
						{
							do 
							{
								printf ("Enter n, m: ");
								fflush (stdin);
								err = scanf ("%d%d", &n, &m);
							} while (err == 0 || n <= 0 || m <= 0);
								table = new double* [n];
							for (i = 0; i < n; i++) 
							{
								
								table[i] = new double [m];
								for (j = 0; j < m; j++) 
								{
									err = 0;
									do
									{
										system ("cls");
										fflush(stdin);
										printf ("Enter element with cord (%d; %d): ", i, j);
										err = scanf ("%lf", &table[i][j]);
									} while (err == 0);
								}
							}
							printf ("The table was filled manually!\n");
							printf ("Press ENTER...\n");
							key = '9';
							getch();
						}
						else if (key == '3')
						{
							char namer[256];
							printf("Enter name file ");
							scanf("%s",namer);
							file = fopen (strcat(namer,".txt"), "r");
							
							if (file != NULL)
							{
								fscanf (file ,"%d", &n);
								fscanf (file ,"%d", &m);
								table = new double* [n];
								for (i = 0; i < n; i++) 
								{
									table[i] = new double [m];
								}
								i = 0;
								while (fscanf (file, "%lf", &table[i/m][i%m]) != 0 && i < n*m-1 && !feof(file)) 
								{
									i++;
									
								}
								if (i == n*m-1)
								{
									printf ("The table was filled from file!\n");
									printf ("Press ENTER...\n");
									key = '9';
								}
								else
								{
									printf ("File's elements invalid!\n");
									printf ("Press ENTER...\n");	
								}
								fclose (file);
							}
							else
							{
								printf ("File invalid!\n");
								printf ("Press ENTER...\n");
							}
							getch();
						}
				}
			} while (key == '9' && error_flag);
		}
		else if (key == '2')
		{
			pr = Process1 (table, n, m);
			if (pr == 0) 
			{
				printf ("Processing error!\n");
			} 
			else if (pr == 1)
			{
				printf ("Success processed!\n");
			} 
			printf ("Press ENTER...\n");
			getch();
		}
		else if (key == '3')
		{
			if (table != NULL) 
			{
				if (pr != 0) 
				{
					printf ("Result:\n\n");	
					sum=Process1 (table, n, m);
					u=Process2 (table, n, m,sum);
					printf ("Max summ absol vel element table   %d\n\n",sum);
					printf ("stroka  %d\n\n",u);	
					printf ("\n");
				}
				else
				{
					printf ("Process please!\n");	
				}
			} 
			else 
			{
				printf ("The table wasn't filled!\n");		
			}
			printf ("Press ENTER...\n");
			getch();
		}
		else if (key == '4')
		{
			if (table != NULL) 
			{
				printf ("Table:\n\n");	
				for (i = 0; i < n; i++) 
				{
					for (j = 0; j < m; j++) 
					{
						printf ("%4.0lf ", table[i][j]);
					}
					printf ("\n");	
				}
				printf ("\n");
			} 
			else 
			{
				printf ("The table wasn't filled!\n");		
			}
			printf ("Press ENTER...\n");
			getch();
		}
		else if (key == '5')
		{
			if (table != NULL) 
			{
				char name[256];
				printf("Enter name file");
				scanf("%s",name);
				file = fopen (strcat(name,".txt"), "w+");
				fprintf(file,"%4.0d%4.0d\n",n,m);
				if (file != NULL)
				{
					for (i = 0; i < n; i++) 
					{
						for (j = 0; j < m; j++) 
						{
							fprintf (file, "%4.0lf ", table[i][j]);
						}
						fprintf (file, "\n");	
					}
					printf ("The table was recorded to a file!\n");
					fclose (file);
				}
				else
				{
					printf ("File invalid!\n");
				}
			}
			else
			{
				printf ("The table wasn't filled!\n");
			}
			printf ("Press ENTER...\n");
			getch();
		}
		
	} while (key != '0' && !error_flag);
	
	if (table != NULL)
	{
		for (i = 0; i < n; i++) 
		{
			delete[] table[i];
		}
		delete[] table;
	}
	
	if (error_flag)
	{
		printf ("An error has occurred!");
	}
	else
	{
		printf ("The program completed successfully!");
	}
	return 0;
}
