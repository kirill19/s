#include <cstdio>
#include <new>
#include "Queue.h"

using namespace std;

const char Queue::ERROR_MESSAGES_FILE[] = "error_messages.txt";

Queue::Queue() : error_code(0)
{
	node *new_node = new (nothrow) node;
	if (new_node)
	{
		new_node->next = NULL;
		first = new_node;
		last = new_node;
		count = 0;
	}
	else
		set_error(ALLOC_ERROR);
}

Queue::Queue(Queue &q) : Queue()
{
	node *new_node, *cur1 = first,
					*cur2 = q.first->next;
	while (!error() && cur2)
	{
		new_node = new (nothrow) node;
		if (new_node)
		{
			new_node->val = cur2->val;
			cur1->next = new_node;
			cur1 = new_node;
			cur2 = cur2->next;
		}
		else
			set_error(ALLOC_ERROR);
	}
	count = q.count;
	cur1->next = NULL;
	last = cur1;
}

Queue::~Queue()
{
	clear();
	delete first;
}

size_t Queue::size()
{
	return count;
}

bool Queue::push(int val)
{
	bool ret = false;
	node *new_node = new (nothrow) node;
	if (new_node)
	{
		new_node->val = val;
		new_node->next = NULL;
		last->next = new_node;
		last = new_node;
		count++;
		ret = true;
	}
	else
		set_error(ALLOC_ERROR);
	return ret;
}

bool Queue::pop()
{
	bool ret = false;
	if (empty())
		set_error(QUEUE_IS_EMPTY);
	else
	{
		node *temp = first->next->next;
		delete first->next;
		first->next = temp;
		if (!temp)
			last = first;
		count--;
		ret = true;
	}
	return ret;
}

bool Queue::pop(int *val)
{
	bool ok = front(val);
	if (ok)
		ok = pop();
	return ok;
}

bool Queue::front(int *val)
{
	bool ret = false;
	if (empty())
		set_error(QUEUE_IS_EMPTY);
	else
	{
		*val = first->next->val;
		ret = true;
	}
	return ret;
}

bool Queue::back(int *val)
{
	bool ret = false;
	if (empty())
		set_error(QUEUE_IS_EMPTY);
	else
	{
		*val = last->val;
		ret = true;
	}
	return ret;
}

bool Queue::set_front(int val)
{
	bool ret = false;
	if (empty())
		set_error(QUEUE_IS_EMPTY);
	else
	{
		first->next->val = val;
		ret = true;
	}
	return ret;
}

bool Queue::set_back(int val)
{
	bool ret = false;
	if (empty())
		set_error(QUEUE_IS_EMPTY);
	else
	{
		last->val = val;
		ret = true;
	}
	return ret;
}

void Queue::clear()
{
	node *temp, *cur = first->next;
	while (cur)
	{
		temp = cur->next;
		delete cur;
		cur = temp;
	}
	first->next = NULL;
	last = first;
}

bool Queue::empty()
{
	return first == last;
}

void Queue::set_error(int code)
{
	error_code |= code;
}

bool Queue::error(int code)
{
	return (error_code & code) == code;
}

int Queue::error()
{
	return error_code;
}

void Queue::clear_error(int code)
{
	error_code &= ~code;
}

void Queue::clear_error()
{
	error_code = 0;
}

Queue *Queue::from_file(FILE *f)
{
	Queue *q = NULL;
	int val, res;

	if (f && !ferror(f))
		q = new (nothrow) Queue();

	if (q)
	{
		do
		{
			res = fscanf(f, "%d", &val);
			if (res == 1)
				q->push(val);
		} while (!q->error() && !ferror(f) && res == 1);
		if (res != 1 && !feof(f))
			q->set_error(FILE_WRONG_FORMAT);
		if (ferror(f))
			q->set_error(FILE_READ_ERROR);
	}

	return q;
}

void Queue::print_error(int code)
{
	FILE *f = NULL;
	char line[256];
	int i = 1;

	if (code == NO_ERROR)
	{
		puts("There are no errors!");
	}
	else
	{
		printf("Error code = %d\n", code);
		f = fopen(ERROR_MESSAGES_FILE, "r");
		if (f)
		{
			do
			{
				if (fgets(line, sizeof line, f) && (code & i) == i)
					printf("%4d = %s", i, line);
				i *= 2;
			} while (i <= code && !ferror(f) && !feof(f));
			fclose(f);
		}
		else
		{
			printf("Can not read file with error messages \"%s\"!\n", ERROR_MESSAGES_FILE);
			perror("");
		}
	}
}