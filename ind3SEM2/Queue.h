#ifndef INT_QUEUE_H
#define INT_QUEUE_H

#include <cstdio>

class Queue
{
  private:
  	static const char ERROR_MESSAGES_FILE[];

	struct node
	{
		int val;
		node *next;
	};

	node *first;
	node *last;
	int error_code;
	std::size_t count;

	void set_error(int code);
  public:
	enum error_codes
	{
		NO_ERROR = 0,		  
		QUEUE_IS_EMPTY = 1,
		ALLOC_ERROR = 2,	 
		FILE_READ_ERROR = 4, 
		FILE_WRONG_FORMAT = 8 
	};

	Queue();
	Queue(Queue &q);
	~Queue();

	static Queue *from_file(std::FILE *f);
	static void print_error(int code);

	bool push(int);  
	bool pop();		 
	bool pop(int *); 

	bool front(int *); 
	bool back(int *);  

	bool set_front(int); 
	bool set_back(int);  

	std::size_t size();
	void clear(); 
	bool empty();

	bool error(int code); 
	int error(); 

	void clear_error(int code);
	void clear_error();
};

#endif //INT_QUEUE_H
