#include <cstdio>
#include <cstring>
#include "Queue.h"

using namespace std;

int remove_even_numbers(Queue *q)
{
	Queue q2;
	int val, ret;

	while (!q->empty() && !q2.error())
	{
		q->pop(&val);
		if (val % 2 != 0)
			q2.push(val);
	}

	ret = q2.error();
	if (!ret)
	{
		while (!q2.empty() && !q->error())
		{
			q2.pop(&val);
			q->push(val);
		}
		ret = q->error();
	}

	return ret;
}

void print_queue(Queue *q)
{
	Queue q2(*q);
	int val;
	if (!q2.error())
	{
		while (!q2.empty())
		{
			q2.pop(&val);
			printf("%d ", val);
		}
		printf("(size = %zu)\n", q->size());
	}
	else
	{
		puts("Error while printing queue");
		Queue::print_error(q2.error());
	}
}

static bool _from_file(Queue *&q)
{
	char name[256];
	bool ret = false;
	FILE *f = NULL;

	fflush(stdin);
	printf("Enter file name: ");
	fgets(name, sizeof name, stdin);
	if (strlen(name) > 0)
		name[strlen(name) - 1] = '\0'; // remove \n at end
	f = fopen(name, "r");
	if (f)
	{
		Queue *new_q = Queue::from_file(f);
		if (new_q)
		{
			if (new_q->error())
			{
				puts("Queue reading error!");
				Queue::print_error(new_q->error());
				delete new_q;
			}
			else
			{
				if (q)
					delete q;
				q = new_q;
				ret = true;
			}
		}
		else
			puts("Can not create new queue!");
		fclose(f);
	}
	else
		perror("Can not open file");
	
	return ret;
}

int main()
{
	Queue *q = NULL;
	int res;

	_from_file(q);
	if (q) {
		print_queue(q);
		
		res = remove_even_numbers(q);
		if (res)
		{
			puts("Error while removing even numbers!");
			Queue::print_error(res);
		}
		print_queue(q);
	}
		
	getchar();
	return 0;
}
